import {ITask} from './Task' 
export const TASKS: ITask[] = [
    {
        id: 1,
        text: 'Terminar primer tarea',
        day: "2012-02-22",
        reminder: true
    },
    {
        id: 2,
        text: "Investigar sobre boostrap",
        day: "2012-02-22",
        reminder: true
    },
    {
        id: 3,
        text: "Terminar video Modulo 3",
        day: "2012-02-22",
        reminder: false
    }
]