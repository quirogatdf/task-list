import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ITask } from '../../Task';
import {UiService} from '../../service/ui.service';
import { Subscription } from 'rxjs';



@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  @Output() onAddTask: EventEmitter<ITask> = new EventEmitter();
  text: string ="";
  day: string="" ;
  reminder: boolean =false;
  showAddTask: boolean = false;
  subscription?: Subscription;
  constructor(
    private UiService: UiService
  ) { 
    this.subscription = UiService.onToggle().subscribe((value) => this.showAddTask = value);
  }

  ngOnInit(): void {
  }

  onSubmit(){
    if(!this.text){
      alert("Please add a task.")
      return
    }
    const {text, day, reminder,} = this
    const newTask = { text, day, reminder};
    
    this.onAddTask.emit(newTask)
  }
}
