import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {ITask} from '../../Task';
import {TASKS} from '../../mock-tasks';
import {faTimes} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-tasks-item',
  templateUrl: './tasks-item.component.html',
  styleUrls: ['./tasks-item.component.css']
})
export class TasksItemComponent implements OnInit {
  @Input() task: ITask =TASKS[0];
  @Output() onDeleteTask: EventEmitter<ITask> = new EventEmitter;
  @Output() onToggleReminder: EventEmitter<ITask> = new EventEmitter;
  faTimes = faTimes;
  constructor() { }

  ngOnInit(): void {
  }
  onDelete(task: ITask){
    //console.log(task);
    this.onDeleteTask.emit(task);
  }

  onToggle(task: ITask){
    this.onToggleReminder.emit(task);
  }
  

}
